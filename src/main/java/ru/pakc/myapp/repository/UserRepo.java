package ru.pakc.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pakc.myapp.model.User;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {

    User findByPhoneNumber(String phoneNumber);

}
