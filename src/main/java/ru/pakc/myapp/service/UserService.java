package ru.pakc.myapp.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.pakc.myapp.model.User;
import ru.pakc.myapp.repository.UserRepo;

import java.util.Random;


@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {
    private final UserRepo userRepo;

    public boolean createUser(User user){
        String phoneNumber = user.getPhoneNumber();
        if(userRepo.findByPhoneNumber(phoneNumber) != null)
            return false;
        log.info("Создан новый пользователь: {}", user.getName());
        user.setAmount("0");
        user.setCartNumber("0000 0000 0000 0000");
        user.setPassword(generationPin());
        userRepo.save(user);
        return true;
    }

    private String generationPin() {
        Random random = new Random();
        int randomPin = random.nextInt(9999) + 1;
        return Integer.toString(randomPin);
    }

    public User findById(Long id) {
       return userRepo.findById(id).orElse(null);
    }

    public User findByPhoneNumber(String phoneNumber) {
        return userRepo.findByPhoneNumber(phoneNumber);
    }
}
