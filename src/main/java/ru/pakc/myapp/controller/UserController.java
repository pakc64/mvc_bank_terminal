package ru.pakc.myapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.pakc.myapp.model.User;
import ru.pakc.myapp.service.UserService;

@Controller
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("/")
    public String homePage() {
        return "home";
    }

    @GetMapping("/registration")
    public String registrationPage() {
        return "registration";
    }

    @PostMapping("/registration")
    public String logIn(User user, Model model) {
        if (!userService.createUser(user)) {
            model.addAttribute("errorMessage", "Пользователь с таким номером уже зарегистрирован " +
                    user.getPhoneNumber());
            return "registration";
        }
        userService.createUser(user);
        return "redirect:/user_info/";
    }

    @PostMapping("/user_info")
    public String userInformation(String phoneNumber, Model model) {
        User  user = userService.findByPhoneNumber(phoneNumber);
        model.addAttribute("name", user.getName());
        model.addAttribute("amount", user.getAmount());
        model.addAttribute("cartNumber", user.getCartNumber());
        model.addAttribute("password", user.getPassword());
        model.addAttribute("id", user.getId());
        return "user_info";
    }


}
